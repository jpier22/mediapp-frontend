import { Component, OnInit } from '@angular/core';
import { LoginService } from 'src/app/_service/login.service';
import { TOKEN_NAME } from 'src/app/_shared/var.constants';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})
export class PerfilComponent implements OnInit {

  form: FormGroup;
  field: boolean = true;

  constructor(
    private loginService: LoginService
  ) { }

  ngOnInit() {

    let token: any = JSON.parse(sessionStorage.getItem(TOKEN_NAME)).access_token;

    let jwtData = token.split('.')[1]
    let decodedJwtJsonData = window.atob(jwtData)
    let decodedJwtData = JSON.parse(decodedJwtJsonData)

    this.form = new FormGroup({
      'usuario': new FormControl({value: decodedJwtData.user_name, disabled: true}),
      'rol': new FormControl({value: decodedJwtData.authorities[0], disabled: true})
    });

  }

}
