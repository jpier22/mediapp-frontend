import { Paciente } from './paciente';

export class Signo {
    idSigno: number;
    paciente: Paciente;
    fecha: string; //ISODATE 2019-02-10T05:00:00
    temperatura: string;
    pulso: string;
    ritmoRespiratorio: string;
}

export class SignoTable {
    idSigno: number;
    paciente: string;
    fecha: string; //ISODATE 2019-02-10T05:00:00
    temperatura: string;
    pulso: string;
    ritmoRespiratorio: string;
}