import { Component, OnInit, ViewChild, destroyPlatform } from '@angular/core';
import { SignoService } from 'src/app/_service/signo.service';
import { Signo, SignoTable } from 'src/app/_model/signo';
import { MatTableDataSource, MatTable, MatSort, MatPaginator, MatSnackBar } from '@angular/material';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-signos',
  templateUrl: './signos.component.html',
  styleUrls: ['./signos.component.css']
})
export class SignosComponent implements OnInit {

  displayedColumns = ['idSigno', 'paciente', 'temperatura', 'pulso', 'ritmoRespiratorio', 'acciones'];
  dataSource: MatTableDataSource<SignoTable>;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  cantidad: number;

  signoTable: SignoTable[];

  constructor(private signoService: SignoService, public route: ActivatedRoute, private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.signoTable = [];
    this.listar();

    this.signoService.signoCambio.subscribe(data => {

      let signo: SignoTable = <SignoTable>{};
      this.signoTable = [];
      for (let d of data) {
        signo = <SignoTable>{};
        signo.fecha = d.fecha;
        signo.idSigno = d.idSigno;
        signo.paciente = d.paciente.nombres + ' ' + d.paciente.apellidos;
        signo.pulso = d.pulso;
        signo.ritmoRespiratorio = d.ritmoRespiratorio;
        signo.temperatura = d.temperatura;
        this.signoTable.push(signo);
      }

      this.dataSource = new MatTableDataSource(this.signoTable);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });

    this.signoService.mensajeCambio.subscribe(data => {
      this.snackBar.open(data, 'AVISO', {
        duration: 2000
      });
    });
  }

  listar() {
    /*this.signoService.listar().subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.sort = this.sort;
      this.dataSource.paginator = this.paginator;
    });*/
    this.pedirPaginado();
  }

  eliminar(idSigno: number) {
    this.signoService.eliminar(idSigno).subscribe(() => {
      this.signoService.listar().subscribe(data => {
        this.signoService.signoCambio.next(data);
        this.signoService.mensajeCambio.next('SE ELIMINÓ');
      });
    });
  }

  applyFilter(filterValue: string) {
    console.log(filterValue);
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }

  mostrarMas(e: any) {
    this.pedirPaginado(e);
  }

  pedirPaginado(e?: any) {
    let pageIndex = 0;
    let pageSize = 10;

    if (e != null) {
      pageIndex = e.pageIndex;
      pageSize = e.pageSize;
    }

    this.signoService.listarPageable(pageIndex, pageSize).subscribe((data: any) => {
      let signos: Signo[] = data.content;
      this.cantidad = data.totalElements;

      let signo: SignoTable = <SignoTable>{};
      this.signoTable = [];
      for (let d of signos) {
        signo = <SignoTable>{};
        signo.fecha = d.fecha;
        signo.idSigno = d.idSigno;
        signo.paciente = d.paciente.nombres + ' ' + d.paciente.apellidos;
        signo.pulso = d.pulso;
        signo.ritmoRespiratorio = d.ritmoRespiratorio;
        signo.temperatura = d.temperatura;
        this.signoTable.push(signo);
      }

      this.dataSource = new MatTableDataSource(this.signoTable);
      //this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });

  }

}
