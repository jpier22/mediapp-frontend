import { MatSnackBar } from '@angular/material';
import { PacienteService } from 'src/app/_service/paciente.service';
import { SignoService } from 'src/app/_service/signo.service';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Paciente } from 'src/app/_model/paciente';
import { map } from 'rxjs/operators';
import * as moment from 'moment';
import { Signo } from 'src/app/_model/signo';


@Component({
  selector: 'app-signos-edicion',
  templateUrl: './signos-edicion.component.html',
  styleUrls: ['./signos-edicion.component.css']
})
export class SignosEdicionComponent implements OnInit {

  form: FormGroup;
  myControlPaciente: FormControl = new FormControl();
  edicion: boolean;
  id: number;

  pacientes: Paciente[] = [];
  paciente: Paciente = new Paciente();
  signo: Signo = new Signo();

  filteredOptions: Observable<any[]>;

  fechaSeleccionada: Date = new Date();
  maxFecha: Date = new Date();
  temperatura: string;
  pulso: string;
  ritmoRespiratorio: string;
  mensaje: string;

  pacienteSeleccionado: Paciente;

  constructor(private builder: FormBuilder, private pacienteService: PacienteService, private signoService: SignoService,
    private snackBar: MatSnackBar, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {

    this.paciente.nombres = 'd';
    this.paciente.apellidos = 'd';

    this.form = this.builder.group({
      'id': new FormControl(0),
      'paciente': this.myControlPaciente,
      'fecha': new FormControl(new Date()),
      'temperatura': new FormControl(''),
      'pulso': new FormControl(''),
      'ritmoRespiratorio': new FormControl('')
    });

    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
      this.edicion = this.id != null;
      this.initForm();
    });

    this.listarPacientes();

    this.filteredOptions = this.myControlPaciente.valueChanges.pipe(map(val => this.filter(val)));
  }

  initForm() {
    if (this.edicion) {
      //cargar la data del servicio hacia el form
      this.signoService.listarPorId(this.id).subscribe(data => {
        this.signo = data;
        console.log(this.signo);
        this.form = this.builder.group({
          'id': new FormControl(data.idSigno),
          'paciente': this.myControlPaciente,
          'fecha': new FormControl(moment(data.fecha).toDate()),
          'temperatura': new FormControl(data.temperatura),
          'pulso': new FormControl(data.pulso),
          'ritmoRespiratorio': new FormControl(data.ritmoRespiratorio)
        });
      });
    }
  }

  listarPacientes() {
    this.pacienteService.listar().subscribe(data => {
      this.pacientes = data;
    });
  }

  filter(val: any) {
    if (val != null && val.idPaciente > 0) {
      return this.pacientes.filter(option =>
        option.nombres.toLowerCase().includes(val.nombres.toLowerCase()) || option.apellidos.toLowerCase().includes(val.apellidos.toLowerCase()) || option.dni.includes(val.dni));
    } else {
      return this.pacientes.filter(option =>
        option.nombres.toLowerCase().includes(val.toLowerCase()) || option.apellidos.toLowerCase().includes(val.toLowerCase()) || option.dni.includes(val));
    }
  }

  displayFn(val: Paciente) {
    return val && val !== null ? `${val.nombres} ${val.apellidos}` : val;
  }

  seleccionarPaciente(e: any) {
    this.pacienteSeleccionado = e.option.value;
  }

  estadoBotonRegistrar() {
    return (!this.form.value['temperatura'] || !this.form.value['pulso'] || !this.form.value['ritmoRespiratorio'] || !this.form.value['fecha'] || this.form.value['paciente'] === null);
  }

  aceptar() {
    let signo = new Signo();
    signo.idSigno = this.form.value['id'];
    signo.paciente = this.form.value['paciente']; //this.pacienteSeleccionado;
    signo.fecha = this.form.value['fecha'].toISOString();
    signo.temperatura = this.form.value['temperatura'];
    signo.pulso = this.form.value['pulso'];
    signo.ritmoRespiratorio = this.form.value['ritmoRespiratorio'];

    if (this.edicion) {
      this.signoService.modificar(signo).subscribe(() => {
        this.signoService.listar().subscribe(data => {
          this.signoService.signoCambio.next(data);
          this.signoService.mensajeCambio.next('SE MODIFICÓ');
        });
      })
    } else {
      this.signoService.registrar(signo).subscribe(() => {
        this.signoService.listar().subscribe(data => {
          this.signoService.signoCambio.next(data);
          this.signoService.mensajeCambio.next('SE REGISTRÓ');
        });
      });
    }
    this.router.navigate(['signos']);
  }

}
